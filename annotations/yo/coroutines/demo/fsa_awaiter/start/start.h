#ifndef INCLUDED_START_
#define INCLUDED_START_

#include "../../promisebase/promisebase.h"

// no operator co_await, separate Awaiter

//start
class Start
{
    struct State: public PromiseBase<Start, State>
    {};

    std::coroutine_handle<State> d_handle;

    public:
        using promise_type = State;
        using Handle = std::coroutine_handle<State>;

        explicit Start(Handle handle);
        ~Start();

        Handle handle() const;
        void go();
};

inline Start::Handle Start::handle() const
{
    return d_handle;
}

extern Start g_start;

#endif
