#include "digit.ih"

Digit coDigit()
{
    char ch;
    while (cin.get(ch))
    {
        if (isalpha(ch))
            co_await Awaitable{ g_letter, "Digit", ch };
        else if (isdigit(ch))
            cout << "at `" << ch << "' remain in digit\n";
        else
            co_await Awaitable{ g_start, "Digit", static_cast<int>(ch) };
    }
    co_await Awaitable{ g_done, "Digit" };
}
