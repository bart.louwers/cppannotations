Since the coroutine handler's state classes can often use the shown minimal
implementations for its members, it might be attractive to define those
members in a separate base-class, thus simplifying the state class's interface
and implementation.

Looking at the tt(Fibo::State) class, its members tt(initial_suspend,
final_suspend) and tt(unhandled_exception) are good candidates for such a base
class. By defining the base class as a class template, receiving the coroutine
handler's class name and the handler's state class name as its template type
parameters then it can also provide the handler's tt(get_return_object)
member. 

Here is how this base class can be defined. It is used by the coroutine
handler's state classes developed in this chapter:
    verbinsert(-s4 //base demo/promisebase/promisebase.h)
