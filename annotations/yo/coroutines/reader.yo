The essence of the tt(Reader) class is that its tt(State) subclass receives a
value from the coroutine (tt(coRead)) at the coroutine's tt(co_yield)
statement. tt(Reader::State) receives the value that's passed to tt(co_yield)
as argument of its tt(yield_value) member, which stores the received tt(float)
value in its tt(std::optional<float> d_value) data member.

The tt(Reader) class itself must define a constructor receiving a handle to
its tt(State) class , and should define a destructor. Its tt(next) member
simply returns the value that's stored in its tt(State) class to tt(next's)
caller. Here is tt(Reader's) complete header file:
        verbinsert(-s4 //interface demo/readbinary/reader/reader.h)

tt(Reader's) and tt(Reader::State's) members have (except for tt(Reader::next))
very short implementations which can very well be defined inline:
        verbinsert(-s4 //defs demo/readbinary/reader/reader.h)

tt(Reader::next) performs two tasks: it resumes the coroutine, and then, once
the coroutine is again suspended (at its tt(co_yield) statement), it returns
the value stored in the tt(Reader::State) object. 
    hi(promise (coroutine)) It can access its tt(State) class object via
tt(d_handle.promise()), returning the value stored in that object:
        verbinsert(-s4 //next demo/readbinary/reader/next.cc)


