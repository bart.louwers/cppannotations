When nesting a class under a class template it may be desirable to provide
that nested class with free operators, which are bound to the surrounding
class. This situation is encountered when defining a nested iterator for which
a free tt(operator==(iterator const &lhs, iterator const &rhs)) operator is
preferred over a member tt(operator==(iterator const &rhs) const), as the
member does not allow promotions of the lhs operand.

It is possible to define the free tt(operator==) as friend in the nested
class, which then automatically becomes a bound friend. E.g.,
        verbinclude(-as4 examples/bounditer1/main.cc)
    However, this requires an in-class implementation, which should be avoided
as it combines interfaces and implementations which reduces clarity.

But when moving the implementation out of the interface we run into the
problem that tt(operator==) is a function template, which must be declared as
such in the class interface. The compiler suggests to em(make sure the
function template has already been declared and add '<>' after the function
name). But declaring
        verbinclude(-s4 //opequal examples/bounditer2/main.cc)
before the tt(struct String) class template, and then implementing
tt(operator==) as 
        verbinclude(-s4 //code examples/bounditer2/main.cc)
    the compiler still complains, reporting em(declaration of 'operator==' as
non-function).

So how to solve this issue? There are two known ways to solve this
problem. One was suggested by Radu Cosma (teaching assistant of our bf(C++)
course in the 2021-2022 academic year), the other solution is provided in 
section ref(BOUNDCONCEPT).

Radu proposed using an application of em(SFINAE): by defining a free operator
provided with a type uniquely defined by the nested class, which is then
provided with a default value in the free function's implementation, the
compiler automatically selects the appropriate overloaded function. This works
for multiple classes declaring nested classes and for classes defining
multiple nested classes alike. Here is an example of two classes each
declaring a nested class tt(iterator):
        verbinclude(-s4 //classes examples/bounditer3/main.cc)
    Note that the nested classes declare the free functions as function
template specializations.

    Then, for each nested class an implementation of the free operator is
provided. These implementations are function templates 
provided with a template type tt(Type) and a second type which is the uniquely
named tt(int) type of the nested class for which the free operator is
implemented:
        verbinclude(-s4 //code examples/bounditer3/main.cc)
    The tt(main) function illustrates the use of both free operators:
        verbinclude(-s4 //main examples/bounditer3/main.cc)




