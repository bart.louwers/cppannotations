#include <string>
#include <vector>
#include <iostream>

using namespace std;

int main()
{
    cout << "\n"
            "   VECTORS:\n";

    vector<char> vect1;

    vect1.push_back('a');
    vect1.push_back('b');
    vect1.push_back('c');
    vect1.push_back('d');
    vect1.push_back('e');

    cout << 
        "      vect1 org: " << 
            vect1.size() << ' ' << vect1.capacity() << '\n';

    vector<char> vect2{ vect1 };

    cout << 
        "copied to vect2: " << 
            vect1.size() << ' ' << vect1.capacity() << '\n' <<
        "          vect2: " << 
            vect2.size() << ' ' << vect2.capacity() << '\n';

    vector<char> vect3{ move(vect1) };

    cout << 
        " moved to vect3: " << 
            vect1.size() << ' ' << vect1.capacity() << '\n' <<
        "          vect3: " << 
            vect3.size() << ' ' << vect3.capacity() << '\n';


    cout << "\n"
            "   STRINGS:\n";

    string str1;

    str1.push_back('a');
    str1.push_back('b');
    str1.push_back('c');

    cout << 
        "      str1 org: " << str1.size() << ' ' << str1.capacity() << '\n';

    string str2{ str1 };

    cout << 
        "copied to str2: " << 
            str1.size() << ' ' << str1.capacity() << '\n' <<
        "          str2: " << 
            str2.size() << ' ' << str2.capacity() << '\n';

    string str3{ move(str1) };

    cout << 
        " moved to str3: " << 
            str1.size() << ' ' << str1.capacity() << '\n' <<
        "          str3: " << str3.size() << ' ' << str3.capacity() << '\n';
}





