The following concepts define two template type parameters. Their generic
form is 
    verb(    template <typename LHS, typename RHS>
    concept Name = 
        ... requirements ...
        ;)
    When used in compound requirements the compiler deduces the type of the
compound expression, and then uses that type as LHS. In the type requirement
following the compound statement only the RHS type is specified. For example
(using the concept tt(std::same_as) (see below)), to require that a function
tt(fun), receiving an argument of some type tt(Type), returns a
tt(std::string) the following concept can be defined:
        verb(template<typename Type>
    concept StringFun = 
        requires(Type param)
        {
            { fun(param) } -> std::same_as<std::string>;
        };)

itemization(
ittq(assignable_from)(requires that RHS-typed expressions
    can be assigned to LHS-typed expressions;)

ittq(common_reference_with)(requires that both types can be converted to an
    identical (reference) type. The concept is satisfied for two identical
    types, but also for two types where one of them is derived from the
    other. E.g., 
   verbinclude(//commonref examples/commonref.cc)) 

ittq(common_with)(works like the previous concept;)

ittq(convertible_to)(requires that the LHS type can automatically be converted
    to the RHS type:
   verbinsert(//convertible examples/convertibleto.cc))

ittq(derived_from)(requires that the LHS type is derived from the RHS type;)

ittq(equality_comparable_with)(requires that operators tt(operator==) and
    tt(operator!=) are available to compare LHS- and RHS-type variables (in
    any order);)

ittq(same_as)(requires that the LHS type is the same as the RHS type. Note
    that this concept acts rather strictly. For example, tt(std::same_as<long
    int, int>) does not satisfy the requirement. If such a strict equality
    isn't really required then tt(convertible_to) might offer a workable
    alternative;)

ittq(swappable_with)(requires that two objects of possibly different types can
    be swapped. The more restrictive variant requiring objects of identical
    types is named tt(swappable);)

ittq(totally_ordered_with)(requires that two objects of possibly different
    types can be ordered using the operators tt(==, !=, <, <=, >,) and
    tt(>=). The requirements for ordering is hi(ordering: strict)em(strict):
    for any two objects tt(one) and tt(two) either tt(one < two, one == two,)
    or tt(one > two) is true. The more restrictive variant requiring objects
    of identical types is named tt(totally_ordered).)
)
