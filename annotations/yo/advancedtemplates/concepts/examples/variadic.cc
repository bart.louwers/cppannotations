#include <type_traits>

//generic
template <typename ...Types>
struct allIntegralTypes;

template <>
struct allIntegralTypes<>
{
    static bool const value = true;
};
//=

//partial
template <typename First, typename ...Types>
struct allIntegralTypes<First, Types ...>
{
    static bool const value = std::is_integral<First>::value and
                              allIntegralTypes<Types ...>::value;
};
//=

//concept
template <typename ...Types>
concept IntegralOnly = allIntegralTypes<Types ...>::value;

template <IntegralOnly ...Types>
void fun(Types ...types)
{}
//=

int main()
{
    fun(12, 13, 'a', true);
    fun();
//    fun(12.5);            // WC: 12.5 is not an integral type
}
