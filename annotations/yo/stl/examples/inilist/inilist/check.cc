//#define XERR
#include "inilist.ih"

void IniList::check(char const *label)
{
    cout << label << "object " << d_id << ": memory owned by " << d_owner <<
            ". Memory " <<
                (
                    s_memory[d_owner] ? "available" : "NOT AVAILABLE"
                ) << '\n';
}
