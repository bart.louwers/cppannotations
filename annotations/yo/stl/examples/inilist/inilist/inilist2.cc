//#define XERR
#include "inilist.ih"

IniList::IniList(IniList const &other)
:
    d_id(s_count++),            // set the object ID
    d_owner(other.d_owner),     // this object doesn't own the memory
    d_index(other.d_index)      // and use other's memory index
{
    cout << "CC: object " << d_id << " using memory[" << d_index <<
            "], owned by object " << d_owner << '\n';
    check("copy: ");
}
